\documentclass{article}
\usepackage[utf8]{inputenc}


\title{ Data Lineage, MANTA and its SQL Analysis}
\author{Lucie Procházková}
\date{December 2022}
\usepackage{setspace}

\doublespacing
\usepackage{biblatex} %Imports biblatex package
\addbibresource{ref.bib} %Import the bibliography file
\usepackage{listings}
\usepackage{graphicx}
\begin{document}

\maketitle

\section{Introduction}

Data lineage and SQL analysis are two key aspects of data engineering. Data lineage is the tracking of data sources and transformations throughout its lifecycle, while SQL analysis is the process of analyzing structured data using Structured Query Language (SQL). Together, these processes can provide insights into data quality, accuracy, and security.

Data lineage is a term used to describe the path that data takes from its source to its destination. This can include the various transformations and processes that the data undergoes along the way, as well as the different systems and applications that it passes through. The concept of data lineage is important for a number of reasons, including ensuring the quality and integrity of the data, as well as tracing the origins of any errors or inconsistencies that may arise. In this essay we will discuss the various steps involved in tracing the path of data, as well as the tools and techniques that can be used to do so. They may also discuss the importance of data lineage in data governance and compliance, and how it can be used to improve the overall quality and reliability of data within an organization. \cite{ilprints918}

\section {Data Lineage}

Data lineage is a critical component for data-driven businesses, as it provides the ability to track data from its origin to its destination. In essence, it is a data map that shows how data flows through an organization. This is especially important for businesses that rely heavily on data to make decisions and drive results. It includes two sides - business (data) lineage and technical (data) lineage. Business lineage presents data flows on a business-term level. Technical data lineage is created from actual technical metadata and tracks data flows on the lowest level - actual tables, scripts and statements. 

Data lineage provides significant benefits that can improve business intelligence. It helps organizations identify where data is coming from, how it is being used, and where it is going. This helps to ensure that data is accurate and up-to-date. It also makes it easier to identify potential data quality issues and bottlenecks, as well as the potential impact they may have on business processes. \cite{valenta}

Data lineage also helps organizations create better data governance. It provides an understanding of the data landscape and helps identify potential sources of data leakage or misuse. It also helps to ensure that data is used appropriately and that processes are adhering to regulatory and industry standards. Data lineage also helps organizations to comply with regulations, such as the General Data Protection Regulation (GDPR), which requires organizations to be able to trace the origin of data and how it has been processed.

Data lineage also improves data quality by helping to identify incorrect or outdated data. This is particularly important for organizations that rely on large and complex datasets for decisions. By tracking data from its origin to its destination, organizations can quickly identify and correct any issues that may arise.

Finally, data lineage helps to improve operational efficiency. By understanding the flow of data through an organization, organizations can identify and address potential bottlenecks and redundancies. This helps to ensure that data is quickly and accurately available to the right stakeholders, allowing them to make informed decisions and take action.

Overall data lineage is a powerful tool that can help organizations improve their business intelligence. By understanding the flow of data throughout an organization, organizations can ensure data quality, create better data governance, and improve operational efficiency. This can ultimately lead to better decision-making and improved results.

\begin{subsection} {Data Lineage Techniques and Examples}
In this section, we present a few common techniques used to perform data lineage on important datasets.

\begin{subsection} {Pattern-Based Lineage}
This technique performs lineage without dealing with the code used to generate or transform the data. It involves evaluation of metadata for tables, columns, and business reports. Using this metadata, it investigates lineage by looking for patterns. For example, if two datasets contain a column with a similar name and similar data values, it is very likely that this is the same data in two stages of its lifecycle. Those two columns are then linked together in a data lineage chart.

The major advantage of pattern-based lineage is that it only monitors data, not data processing algorithms, and so it is technology agnostic. It can be used in the same way across any database technology, whether it is Oracle, MySQL, or Spark.

The downside is that this method is not always accurate. In some cases, it can miss connections between datasets, especially if the data processing logic is hidden in the programming code and is not apparent in human-readable metadata.
\end{subsection}

\begin{subsection}{Lineage by Data Tagging}
This technique is based on the assumption that a transformation engine tags or marks data in some way. In order to discover lineage, it tracks the tag from start to finish. This method is only effective if you have a consistent transformation tool that controls all data movement, and you are aware of the tagging structure used by the tool.

Even if such a tool exists, lineage via data tagging cannot be applied to any data generated or transformed without the tool. In that sense, it is only suitable for performing data lineage on closed data systems.
\end{subsection}
\begin{subsection}{Self-Contained Lineage}
Some organizations have a data environment that provides storage, processing logic, and master data management (MDM) for central control over metadata. In many cases, these environments contain a data lake that stores all data in all stages of its lifecycle.

This type of self-contained system can inherently provide lineage, without the need for external tools. However, as with the data tagging approach, lineage will be unaware of anything that happens outside this controlled environment.
\end{subsection}
\cite{learning}

\begin{subsection}{Parsed data lineage}
    Last technique could be called Parsed data lineage. 
   Data lineage analysis is done by parsing, or analyzing the structure and possibly content of the data, as well as the associated metadata, to identify key information about its origins, transformations, and actions.

Parsed data lineage can provide more detailed and accurate information about data lineage than manual methods, and can be used to identify potential sources of errors or inconsistencies in data. It can also help organizations automate their data lineage tracking processes, making it easier to manage and analyze large amounts of data.

The major advantage of this method is its accuracy. The major disadvantage is its complicated inhouse implementing, however, there are tools that handle this issue. 


\end{subsection}
\end{subsection}
\section {Data lineage tools}

There are several tools that can be used for data lineage, depending on the specific needs and requirements of an organization. Some common tools for data lineage include:

\begin{itemize}
    \item Data mapping tools: These tools are used to create visual diagrams that show the flow of data between different systems and applications. These diagrams can be used to trace the path of data, and to identify any potential bottlenecks or issues that may arise.
    \item Data profiling tools: These tools are used to analyze the data itself, in order to identify any inconsistencies or errors. They can be used to check for data quality issues, and to identify any potential problems that may need to be addressed.
    \item   Data governance tools: These tools are used to manage the overall quality and integrity of data within an organization. They can be used to set rules and policies for data usage, and to monitor and enforce compliance with these rules.
    \item Data integration tools: These tools are used to connect different systems and applications, and to facilitate the flow of data between them. They can be used to ensure that data is properly synchronized and updated across all systems, and to help prevent data loss or corruption.
\end{itemize}

\begin{section} {Manta}

MANTA is a data lineage platform that enables organizations to trace the path of their data from its source to its destination. The platform allows users to create visual diagrams that show the flow of data between different systems and applications, and to track the various transformations and processes that the data undergoes along the way. This helps ensure the quality and integrity of the data, and can also be used to identify and correct any errors or inconsistencies that may arise. On figure \ref{fig:adminui} you can see example on how administration UI of MANTA looks like.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{img/adminUI.png}
    \caption{Admin UI of MANTA Flow\cite{manta_2022}}
    \label{fig:adminui}
\end{figure}

One of the most important features of MANTA is that it does not analyze the data itself. MANTA does not even read the data. All of the analysis is done purely based on the metadata, structural information about the data and its usage. 

Another one of the key features of MANTA is its ability to integrate with a wide range of different systems and applications. This allows users to track the flow of data across their entire organization, regardless of the specific technologies and platforms that are in use. The platform also includes a range of powerful tools for data profiling and governance, which can be used to ensure that data is properly managed and controlled throughout its lifecycle.

In addition to its core data lineage capabilities, MANTA also offers a range of additional features and tools that can be used to enhance the overall quality and reliability of an organization's data. For example, the platform includes tools for data quality management, which can be used to identify and correct any issues with the data. It also includes tools for data governance, which can be used to set rules and policies for data usage, and to monitor and enforce compliance with these rules.

Overall, MANTA is a powerful and versatile data lineage platform that can help organizations improve the quality and reliability of their data. By providing a clear and detailed view of the path that data takes from its source to its destination, the platform allows users to identify and correct any potential issues, and to ensure that data is properly managed and controlled throughout its lifecycle.

MANTA works with number of technologies, including various databases such as Oracle, PostreSQL, MSSQL, Snowflake, Google BigQuery, Amazon Aurora, DB2, Amazon Redshift, GreenPlum, Netezza, Teradata or SAP Hana, but also programming languages such as Cobol, or reporting tools like SAS, Power BI, Tableau, Excel, OBIEE or Cognos. 

This list is non exhaustive and it keeps growing. For example in my diploma thesis, I am working on implementing new scanner of Databricks SQL for MANTA platform.

MANTA Flow is made up of three major components.

MANTA Flow CLI—Java command line application that extracts all scripts from source databases and repositories, analyzes them, sends all gathered metadata to the MANTA Flow Server, and optionally, processes and uploads the generated export to a target metadata database

MANTA Flow Server—Java server application that stores all gathered metadata inside its metadata repository, transforms it to a form suitable for import to a target metadata database, and provides it to its visualization or third-party applications via API

MANTA Admin UI (runs on the MANTA Flow Service Utility application server)—Java server application providing a graphical and programming interface for installation, configuration, updating, and overall maintenance of MANTA Flow (detailed documentation can be found on the page MANTA Admin UI).


In MANTA first technology-specific scanners parse code (such as stored procedures, ETL job definitions, etc.) and produce lineage from the metadata that defines the structures and movements of information throughout your ecosystem.

First step is to connect to the system, and from that extract all scripts and informations needed to analyze the data flow. These information may vary depending on the analyzed technology.  In the case of SQL, we usually extract all DDL scripts, builtin and custom functions, metadata about the system as it is usually important later in the process. \cite{manta_2022}

\begin{subsection}{Extraction process}
There exist two ways of extracting metadata from databases in general. The first way is to extract metadata information from defined views or catalogs
provided by a database engine. However, some databases do not have any views or functions providing these metadata. In such a case, there is a second way of obtaining objects metadata by extracting all DDL scripts of needed database objects. After the extraction, the DDL scripts are parsed and analyzed where during semantic analysis, all declarations are created in the database dictionary. This way of extracting metadata is much more complicated and time-consuming. For example, the MANTA Snowflake scanner supports the concept of INFORMATION\_SCHEMA, so there is no reason for us to extract all DDL scripts \cite{marek2020extrakce}.

INFORMATION SCHEMA is a series of views that provide access to: 

\begin{itemize}
    \item dataset metadata,
    \item routine metadata,
    \item table metadata,
    \item view metadata.
\end{itemize}

This way of extraction allows accessing metadata in a programmatical way with the help of SQL queries. The problem is that list above does not contain an ability to extract metadata about projects which are required for a successful and complete process of extraction. The INFORMATION SCHEMA feature was in the beta release phase when working on this thesis and might be changed in future releases. \cite{kyrylo2021analyza}

For the extraction process it is needed to explore needed roles for the extraction which may vary based on the underlying technology.  

    
\end{subsection}

Static SQL analysis can be used to identify and analyze the performance of SQL statements used to create and manage databases. This process can help database administrators to identify errors, optimize queries, and better understand the database structure.

Next step is to parse the SQL statements: This involves breaking down the SQL statements into their individual components, such as keywords, operators, and identifiers, in order to understand their structure and meaning. In MANTA, we use ANTLR for this. During this step, validating SQL is usually performed. Any errors or issues with the SQL syntax will be identified and flagged during this step.


\begin{subsection} {SQL Analysis}

Second step in the data lineage analysis is the actual SQL analysis.  Static SQL analysis is a process used to identify and analyze the performance of SQL statements used to create and manage databases. It is a method of analyzing the syntax of the SQL statements in a database. 

\begin{subsection} {ANTLR}
ANTLR (ANother Tool for Language Recognition) is a powerful parser generator that allows you to write parsers for small or large grammars easily. It is written in Java and supports many other languages, including C++, C\#, and Python.

ANTLR works by taking a grammar as input and automatically generating the code for a parser that can recognize the language described by the grammar. The generated parser can then parse input sentences and perform whatever actions are specified in the grammar.

Here's a simple example of how ANTLR works:

Suppose you want to write a parser for a simple programming language. You would first write a grammar for the language, which specifies the syntax of the language. The grammar might look something like this:

\begin{lstlisting}
grammar Simple;

program
 : statement+
 ;

statement
 : 'print' STRING ';'
 ;

STRING
 : '"' (~'"')* '"'
 ;

 \end{lstlisting}
This grammar defines a program as one or more statements, and a statement as the keyword print followed by a STRING and a semicolon. A STRING is defined as a sequence of characters enclosed in double quotes.

Once you have written the grammar, you can use ANTLR to generate the parser. The generated parser will be a Java class you can use in your own programs. To use the parser, you would create an instance of the parser class, and then call one of its methods to parse an input string.

For example, suppose you have the following input string:

\begin{lstlisting}
    print "Hello, world!";
\end{lstlisting}

To parse this string, you would create an instance of the generated parser class and call its parse() method, passing the input string as an argument. The parse() method would then use the grammar you wrote to parse the input string and perform whatever actions are specified in the grammar. In this case, the parse() method would recognize the print statement and print "Hello, world!" to the console. \cite{antlr_2022}

The result of parsing is a AST Tree. Example of that tree can be seen at figure \ref{fig:ast}.
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{img/AST_example.png}
    \caption{Example of AST \cite{jan2022analyza}}
    \label{fig:ast}
\end{figure}
\end{subsection}

Next step is analyzing the SQL semantics: This involves examining the meaning and intent of the SQL statements, in order to determine whether they are logically correct and will produce the desired results. This step may involve analyzing the relationships between different tables and columns, as well as checking for any potential conflicts or inconsistencies in the SQL statements.



When we do have resolved and analyzed AST tree, which is the result of the previous steps, we continue with the dataflow analysis.  
\end{subsection}


\subsection {Dataflow analysis} 

In the dataflow analysis, each node in the AST is visited and analysis is performed. We create graph capturing the dataflow. It is stored in Neo4j graph database and can later be filtered or visualised.  \cite{manta_documentation} 

There are several ways of representing data lineage, but a graph representation is generally recongnised the best because of its clarity. In the graph representation is called a data flow graph. It is an oriented graph that consists of nodes and oriented edges. The nodes represent the actual data - column, table etc. Two nodes can be connected by the oriented edges representing data flows between them. There
exist two types of data flow edges:
\begin{itemize}
    \item  Direct flows
    \item  Filter flows
\end{itemize}
Direct data flows indicate that the source nodes directly participate in the data origin of target nodes. An example of this is a SELECT statement
because the data from the source table from where the statement reads the data directly appear in the result of the SELECT operation.
Filter data flows affect the content of the target node without directly contributing to it. The example of filter flows is a WHERE clause of the SELECT statement which only restricts the value set of the SELECT operation. Another example of this can be for example ORDER BY clause, which again only affects the order of the data, not its content. \cite{marek2020extrakce}



You can see how does the final lineage look like on figures \ref{fig:dataflow1}, \ref{fig:dataflow2} and \ref{fig:dataflow3}.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{img/dataflow.png}
    \caption{Example of dataflow\cite{manta_2022}}
    \label{fig:dataflow1}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{img/dataflow2.png}
    \caption{Example of dataflow\cite{manta_2022}}
    \label{fig:dataflow2}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{img/dataflow3.png}
    \caption{Example of dataflow\cite{manta_2022}}
    \label{fig:dataflow3}
\end{figure}

\end{section}

\section {Conclusion}
In conclusion, data lineage is an important concept that is used to trace the path of data from its source to its destination. By tracking the various transformations and processes that data undergoes, as well as the systems and applications that it passes through, organizations can ensure the quality and integrity of their data, and can identify and correct any errors or inconsistencies that may arise. MANTA is a powerful data lineage platform that provides a range of tools and features for tracing the flow of data within an organization, and for ensuring that data is properly managed and controlled throughout its lifecycle. 
For dataflow analysis MANTA uses only metadata, data about the structure of the data, not its content. For analyzing dataflow in SQL databases, it uses static SQL analysis consisting of parsing, resolving and dataflow graph generating.
For parsing, it uses the ANTLR library. 
By using data lineage and platforms like MANTA, organizations can improve the quality and reliability of their data, and can gain a better understanding of the flow and usage of their data.

\printbibliography %Prints bibliography
\end{document}
